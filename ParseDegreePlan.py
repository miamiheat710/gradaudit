from docx import Document
import argparse
import os


def print_guided(guided_electives_file):
    guided_electives = []
    document = Document(guided_electives_file)
    table = document.tables[0]
    for row in table.rows:
        text = row.cells[0].paragraphs[0].text
        split = text.split(" ")
        if len(split) < 2:
            continue
        disciplines = [s.lower() for s in split[0].split("/")]
        number = split[1]
        for discipline in disciplines:
            guided_electives.append(discipline+number)

    return guided_electives


def print_science():
    with open("ScienceElectives.txt", "r") as science:
        return science.read()


def print_core(fil, grad_fil, aud_fil):
    with open("CoreCurriculum.txt", "r") as core:
        fil.write("\n")
        fil.write(core.read())
    with open("CoreCurriculum.txt", "r") as core:
        for line in core.readlines():
            if "required" in line:
                course = line.split("(")[1].split(",")[0]
                grad_fil.write(",\n\t_hasTaken(Student, " + course + ")")
                aud_fil.write(",\n\t_t(Student, " + course + ")")


def write_grad_rule(rules_fil, disciplines_to_paths, elective_hours):
    rules_fil.write("ableToGraduate(Student):-\n")
    rules_fil.write("\t_metRequiredCourses(Student)")
    for d in disciplines_to_paths:
        rules_fil.write(",\n\t_met" + d.capitalize() + "Courses(Student)")
    rules_fil.write(",\n\t_meetsHours(Student,'Language, Philosophy and Culture',3)")
    rules_fil.write(",\n\t_meetsHours(Student,'Creative Arts',3)")
    rules_fil.write(",\n\t_meetsHours(Student,'American History',6)")
    if "Free Elective" in elective_hours:
        rules_fil.write(",\n\t_meetsElectiveHours(Student, " + str(elective_hours["Free Elective"]) + ")")
        elective_hours.pop("Free Elective")
    for e in elective_hours:
        rules_fil.write(",\n\t_meetsHours(Student,'" + e + "'," + str(elective_hours[e]) + ")")
    rules_fil.write(".\n\n")


def write_audit_rule(rules_fil, disciplines_to_paths, elective_hours):
    rules_fil.write("_audit(Student):-\n")
    rules_fil.write("\t_auditRequiredCourses(Student)")
    for d in disciplines_to_paths:
        rules_fil.write(",\n\t_audit" + d.capitalize() + "Courses(Student)")
    rules_fil.write(",\n\t_auditHours(Student,'Language, Philosophy and Culture',3)")
    rules_fil.write(",\n\t_auditHours(Student,'Creative Arts',3)")
    rules_fil.write(",\n\t_auditHours(Student,'American History',6)")
    if "Free Elective" in elective_hours:
        rules_fil.write(",\n\t_auditElectiveHours(Student, " + str(elective_hours["Free Elective"]) + ")")
        elective_hours.pop("Free Elective")
    for e in elective_hours:
        rules_fil.write(",\n\t_auditHours(Student,'" + e + "'," + str(elective_hours[e]) + ")")
    rules_fil.write(".\n\n")


def main():
    parser = argparse.ArgumentParser('Writes <bs/ba>/<major>/<year> folder\'s contents')
    parser.add_argument('degree_type', choices=['bs', 'ba'])
    parser.add_argument('major')
    parser.add_argument('degree_year')
    args = parser.parse_args()

    degree, major, year = args.degree_type, args.major, args.degree_year
    directory = "%s/%s/%s/" % (degree, major, year)
    academic_year = str(int(year)-2000) + "." + str(int(year)-1999)
    degree_plan = directory + major.upper() + "-DP-" + academic_year + ".docx"
    guided_electives = directory + "Options-for-" + major.upper() + "-Guided-Electives-" + academic_year + ".docx"
    document = Document(degree_plan)
    printed_guided = False
    tables = document.tables
    current = ""
    table_number = 1
    print_at_end = []
    disciplines_to_paths = {}
    prefix = "%s%s" % (major, year)
    first_line = True
    reqFile = prefix + 'req.lp'
    req2File = prefix + 'req2.lp'
    gradFile = prefix + 'grad.lp'
    rulesFile = prefix + 'rules.lp'
    auditFile = prefix + 'audit.lp'
    elective_hours = {}
    with open(directory + reqFile, 'w') as fil, open(directory + gradFile, 'w') as grad_fil, open(directory + auditFile, 'w') as aud_fil, open(directory + rulesFile, 'w') as rule_fil:
        grad_fil.write("_metRequiredCourses(Student):- \n")
        aud_fil.write("_auditRequiredCourses(Student):- \n")
        rule_fil.write("#include \'" + reqFile + "\'.\n")
        rule_fil.write("#include \'" + req2File + "\'.\n")
        rule_fil.write("#include \'" + gradFile + "\'.\n")
        rule_fil.write("#include \'" + auditFile + "\'.\n\n\n")

        for table in tables:
            if table_number == 1:
                fil.write("%courses for major preparatory requirements\n")
            if table_number == 2:
                for line in print_at_end:
                    fil.write(line)

                print_at_end = []
                fil.write("\n%courses for major core requirements\n")
            if table_number == 3:
                for line in print_at_end:
                    fil.write(line)
                print_core(fil, grad_fil, aud_fil)
                table_number += 1
                continue
            if table_number == 4:
                for row in table.rows:
                    if len(row.cells) > 3:
                        for paragraph in row.cells[3].paragraphs:
                            if "Free" in paragraph.text and "Elective" in paragraph.text:
                                if "Free Elective" in elective_hours:
                                    elective_hours["Free Elective"] += int(row.cells[0].paragraphs[0].text)
                                else:
                                    elective_hours["Free Elective"] = int(row.cells[0].paragraphs[0].text)
                break
            for row in table.rows:
                if len(row.cells) > 4:
                    for paragraph in row.cells[3].paragraphs:
                        text = paragraph.text
                        if "Science" in text and "Elective" in text:
                            if 'Science Elective' in elective_hours:
                                elective_hours['Science Elective'] += int(row.cells[0].paragraphs[0].text)
                            else:
                                elective_hours['Science Elective'] = int(row.cells[0].paragraphs[0].text)
                            print_at_end.append("\n")
                            print_at_end.append(print_science())
                    for paragraph in row.cells[4].paragraphs:
                        text = paragraph.text
                        discipline = text.strip().split(" ")[0].lower()
                        numbers = [int(s) for s in text.replace("*", "").split(" ") if s.isdigit()]
                        if "XX" in text:
                            if 'Guided Elective' in elective_hours:
                                elective_hours['Guided Elective'] += int(row.cells[1].paragraphs[0].text)
                            else:
                                elective_hours['Guided Elective'] = int(row.cells[1].paragraphs[0].text)
                            if printed_guided:
                                continue
                            else:
                                print_at_end.append(
                                    "\n%these classes are the guided elective options for " + major + "\n")
                                for option in print_guided(guided_electives):
                                    print_at_end.append("_req(" + option + ", \'Guided Elective\').\n")
                                printed_guided = True
                                continue
                        if len(numbers) < 1:
                            continue
                        if "or" in text:
                            if discipline != current:
                                print_at_end.append("\n%courses for " + discipline + " credit\n")
                                disciplines_to_paths[discipline] = []
                                current = discipline
                            for number in numbers:
                                print_at_end.append("_req(" + discipline + str(number) + ", " + discipline + ").\n")

                            disciplines_to_paths[discipline].append(numbers)

                        else:
                            if current != "":
                                current = ""
                            if len(numbers) == 1:
                                fil.write("_req(" + discipline + str(numbers[0]) + ", required).\n")
                                if first_line:
                                    grad_fil.write("\t_hasTaken(Student, " + discipline + str(numbers[0]) + ")")
                                    aud_fil.write("\t_t(Student, " + discipline + str(numbers[0]) + ")")
                                    first_line = False
                                else:
                                    grad_fil.write(",\n\t_hasTaken(Student, " + discipline + str(numbers[0]) + ")")
                                    aud_fil.write(",\n\t_t(Student, " + discipline + str(numbers[0]) + ")")

            table_number += 1

        grad_fil.write(".")
        aud_fil.write(".")
        for d in disciplines_to_paths:

            path_options = list(set(zip(*disciplines_to_paths[d])))

            for path in path_options:

                grad_fil.write("\n\n_met" + d.capitalize() + "Courses(Student):- ")
                aud_fil.write("\n\n_audit" + d.capitalize() + "Courses(Student):- ")
                first_line = True

                for course in path:
                    if first_line:
                        grad_fil.write("\n\t_hasTaken(Student, " + d + str(course) + ")")
                        aud_fil.write("\n\t_hasTaken(Student, " + d + str(course) + ")")
                        first_line = False
                    else:
                        grad_fil.write(",\n\t_hasTaken(Student, " + d + str(course) + ")")
                        aud_fil.write(",\n\t_t(Student, " + d + str(course) + ")")
                for course in path:
                    grad_fil.write(",\n\t_takenFor(Student, " + d + str(course) + ", " + d + ")")
                    aud_fil.write(",\n\t_takenFor(Student, " + d + str(course) + ", " + d + ")")

                grad_fil.write(".")
                aud_fil.write(".")

            first_line = True
            aud_fil.write("\n\n_audit" + d.capitalize() + "Courses(Student):- ")
            for course in path_options[0]:
                if first_line:
                    aud_fil.write("\n\t_t(Student, " + d + str(course) + ")")
                    first_line = False
                else:
                    aud_fil.write(",\n\t_t(Student, " + d + str(course) + ")")
            for course in path_options[0]:
                aud_fil.write(",\n\t_takenFor(Student, " + d + str(course) + ", " + d + ")")
            aud_fil.write(".")

        write_grad_rule(rule_fil,disciplines_to_paths,elective_hours)
        write_audit_rule(rule_fil, disciplines_to_paths, elective_hours)
        rule_fil.write("_main(Student):-\n")
        rule_fil.write("\tableToGraduate(Student).\n\n")
        rule_fil.write("_main(Student):-\n")
        rule_fil.write("\t_audit(Student).")
        os.system("python req_writer.py " + degree + " " + major + " " + year)


if __name__ == "__main__":
    main()