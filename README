Graduation Audit System
README

Saikiran Srirangapalli, Arman Sobhi,
Chris Janusa, Nymisha Jahagirdar

Contents

1. Description
2. License
3. Package Contents
4. Dependencies
5. How To Run an Audit
6. Automatically Generating Degree Plans
7. Creating/Modifying Degree Plans
8. Useful Predicates

1. Description

This program is a degree audit system for degrees at The University of Texas at Dallas. It can determine if a student can graduate and will determine which courses a student should take if they can't graduate. Audits can be performed for degrees at other universities by updating courses.lp and implementing the degree plans.

2. License

The Graduation Audit System is distributed under the GNU Public License Version 3.

3. Contents

bs/ 			-degree plans for Bachelor of Sciences degrees
ba/			-degree plans for Bachelor of Arts degrees
transcripts/		-Some example transcripts in the specified .csv format
README			-this README file
counting.lp		-file that has rules for counting hours
course.lp		-file that contains every class in the system
COPYING			-License
main.lp			-file that is run by s(ASP)
main_writer.py		-Python script that generates main.lp
req_writer.py		-Python script that generates req.lp file
extract.py              -Python script that can parse unofficial UTD transcripts from Galaxy
ParseDegreePlan.py 		-Python script that can parse degree plans that are available in .docx format detailed in section 6


4. Dependencies

This system uses s(ASP). s(ASP) and its installation instructions can be downloaded from http://sourceforge.net/projects/sasp-system/

Python (2 or 3) is required to run the three python scripts.

Ghostscript is required to run extract.py.

The docx library for Python is required for ParseDegreePlan.py. 

5. How To Run an Audit

Example degree plans and transcripts are included in the bs/, ba/, and transcripts/ folders. 

To run an audit, you must use main_writer.py and pass in a transcript file to create a main.lp file which will be executed by s(ASP). Run this command in the terminal:

		python main_writer.py transcripts/saics.csv
		
This will use transcripts/saics.csv to build main.lp, which can in turn be executed by s(ASP). Once the main.lp file is created run this command in the terminal:
		
		sasp main.lp

This will run the audit and provide appropriate advising information.

6. Automatically Generating Degree Plans 

If the degree plan and guided electives are available in a .docx format, we can automatically generate the req, req2, rules, grad, and audit files.

The expected format can be seen in the degree plans seen here: https://engineering.utdallas.edu/academics/undergraduate-majors/undergrad-advising/current-students/

A. Download the degree plan document. The naming convention of this file should be
		<acronym>-DP-<year>.docx
		ex: CS-DP-18.19.docx

B. Download the guided electives document. The naming convention of this file should be
		Options-for-<acronym>-Guided-Electives-<year>.docx
		ex: Options-for-CS-Guided-Electives-18.19.docx

C. Note the year, type of degree (BS or BA), and the simple acronym for the degree used in the names of the downloaded docx files (ex. Computer Science is CS), and create the corresponding directory path:
		/gradaudit/<type of degree>/<acronym>/<year>
		ex: /gradaudit/bs/cs/2018

D. Move the two downloaded files from steps A and B to the directory created in step C.

E. Run the python script ParseDegreePlan.py and pass three arguments in the command line in this form:
		python ParseDegreePlan.py <type of degree> <acronym> <year>
		ex: python ParseDegreePlan.py bs cs 2018

F. Now the 5 files, req, req2, rules, grad, and audit, are created in the same directory.

G. All the needed files have been automatically generated, and the user can now create the main.lp file (See Section 5: How To Run An Audit). Section 6 can be refered to as to modify the degree plan.

7. Creating/Modifying Degree Plans

If the degree plan could not be found in the .docx format as needed in section 5, we must create the necessary files manually.

The system allows for custom degree plans with relative ease. 

A. First, choose a simple acronym for the degree. For example, Computer Science could be cs. Also, make note of the year and the type of degree (BS or BA).

B. Next, create a "req" lp file with the naming convention:

			"<acronym><year>req.lp"
			
	For example, for the CS 2015 degree plan, the file would be called: cs2015req.lp.
	
C. Now, create logic rules in the form of:

		_req(className,typeOfRequirement).
		
className is the name of the class. (i.e. cs1337).

typeOfRequirement is what type of requirement the class will fulfill.
 
Note: If the class is absolutely required to graduate, its typeOfRequirement is "required". Otherwise, it is whatever requirement you want to name it. 

Example: hist1301 is an American history class that counts toward the American History Requirement in the degree plan and it is not absolutely required to graduate (another American History course can be used to satisfy the requirement).

Add: _req(hist1301,americanHist).

Another example: govt2305 is a government class that is required to graduate.

Add: _req(govt2305,required).

For every class that could be used for a particular type of requirement in the degree plan, add a _req() rule.
These rules are used to associate classes with requirements and used for counting.

Once the req file is complete, make sure the naming convention is correct and place it in the correct directory based on type of degree, acronym, and year.

i.e. For the B.S. Computer Science degree plan of 2015, place the req file in gradaudit/bs/cs/2015. This directory will eventually contain 5 LP files all associated with this exact degree plan.

Note: A course should not satisfy more than one degree requirement.
i.e. there should not be the two facts '_req(course1,req1)' and '_req(course1,req2)' for any degree.

D. Once the _req file has been created, run the python script req_writer.py and pass three arguments in the command line in this form:

	python req_writer.py <type of degree> <acronym> <year>
	
	ex: python req_writer.py bs cs 2015

This will create a req2.lp file in the same directory as the first one. 

E. Next, create a new lp file with the naming convention
		<acronym> <year> grad.lp
		ex: cs2015grad.lp
	This file will contain the rules to see if the student can graduate and will fail if the student is missing a class required. Use the predicate _hasTaken(Student,<className>). It is recommended for the rules to start with the same keyword such as _met.
	
	Example:
		_metRequiredClasses(Student):- _hasTaken(Student,cs1337),
									   _hasTaken(Student,cs2336).
		
	As degree plans become more complicated, the logic rules will become more complex. Here are some ways to deal with complex cases:
	
		Take a certain combination of a group of classes:
			For example, take class A and C or take class B or D to satisfy this requirement. Taking class A and B will not satisfy this requirement.
			
			Create a new predicate with more than one way to be true. If the first one fails, then the system will try the next one until one either becomes true or all are false.
		
		A specialization must be met:
			See se2015grad or math2015grad. These degree plans require a student take classes that satisfy one of many specializations.
			
	The grad file is complete when the required classes rule and specialization rules are implemented.
	
F. Next, create a new lp file with the naming convention 
		<acronym> <year> audit.lp
		ex: cs2015audit.lp
		
	This file is almost identical to grad.lp except that instead of _hasTaken, use _t as the predicate. _t is a predicate that will always be true and will show if a necessary class is not taken. These rules will only run if the student cannot graduate. To create this, copy and paste the grad file, change the predicate name and all the _hasTaken(Student,className) to _t(Student,className) ONLY for the classes that must be taken. It is recommend to have the same keyword such as _audit.
	
	Ex: _metRequiredCourses(Student):- _hasTaken(Student,cs1337). %for grad
		_auditReuiredCourses(Student):- _t(Student,cs1337). %for audit
		
	For special cases, the user will have to decide how to encode the changes. The main importance of the audit file is that it is almost identical to the grad file except that it will always be true.
	
	See math2015audit.lp and se2015audit.lp for special cases.
	
G. The last file to create is the rules file and has the naming convention
			<acronym><year>rules.lp
			ex: cs2015rules.lp
		
		This file contains the rules to determine if the student can graduate and if the student cannot, will show what classes are needed.
		
		Every rules file needs these 4 include statements
		
		#include '<acronym><year>req.lp'.
		#include '<acronym><year>req2.lp'.
		#include '<acronym><year>grad.lp'.
		#include '<acronym><year>audit.lp'.
		
		ex:
		
		#include 'cs2015req.lp'.
		#include 'cs2015req2.lp'.
		#include 'cs2015grad.lp'.
		#include 'cs2015audit.lp'.
		
		Next, create two predicates similar to ableToGraduate and _audit.
		ableToGraduate will use the rules found in the grad file and _audit will use the rules in the audit file. These predicates contain rules that describe how to graduate and contain predicates such as _meetsHours and _auditHours (See Useful Predicates Section). Once those two predicatse are complete, create two _main(Student) predicates, one with the ableToGraduate rule and the other with the _audit rule as its body. This will attempt to check ableToGraduate first, then if it fails, _audit will be run and will always be true.
		
		See se2015rules.lp for reference and more documentation.
		
H. Finally, all 5 files are created and the user can now create the main.lp file (See How To Run An Audit).

Note: course.lp should contain all valid courses along with the number of credit hours they can be taken for. We also assume courses that can count for a variable number of hours count for 3 hours (e.g. CS4V98).
	_course(className,hours) where hours is the credit hours.	

8. "counting.lp" contains useful predicates to help code graduation rules


_hasTaken(Student,Course).
	This rule is automatically generated as a fact when a class has been taken by a student from the CSV transcript. It denotes that "Student has taken this Course".
	
	Example: _hasTaken(bob,cs1337). %bob has taken cs1337
	
hasNotTaken(Student,Course).
	This rule is true when not _hasTaken(Student,Course) is true. It denotes that "Student has not taken this Course."
	
	Example: hasNotTaken(bob,cs2305). %bob has not taken cs2305
	
_t(Student,Course).
	This rule uses an even loop so that it will always be true regardless whether or not the student has taken the course. If the student has not taken the course, the answer set will show the hasNotTaken predicate in the answer set. If the student has taken the course, it will hide the _hasTaken and _t predicates. 
	
	Example: _t(bob,cs3305) %if bob has not taken cs3305, note it in the answer set but return true regardless.
	
_takenFor(Student,Course,Req).
	This rule is used to indicate that a Student has used this Course for a certain Req. This should be used to indicate a class cannot be used for an elective credit.
	
_takenForElective(Student,Course).
	This rule is used to indicate that a Student has used this Course as an elective credit.
	
_meetsHours(Student,Req,Min)
	This rule will count the amount of credit hours a Student has taken of a certain Req and will be true if the amount of credit hours is greater than the Min. 
	Req is the name of requirement found in the req file.
	Min is a number of hours needed to fufill a requirement.
	
	Example: _meetsHours(bob,americanHist,6). %if bob has 6 or more hours of classes that meet the requirement for american histrory, then the pridcate is true.
	
_auditHours(Student,Req,Min).
	This rule will is the same as meetsHours except it will always be true and if the total hours is less than the Min, needsHours(Student,Req,Diff) will appear in the answer set, with Diff as the hours needed in the requirement.
	
_meetsElectiveHours(Student,Min).
	This rule is the same as meetsHours except the requirement is for electives. Min is the amount of electives needed.
	
_auditElectiveHours(Student,Min).
	This rule is the same as auditHours except the requirement is for electives. Similar to auditHours, it will always be true.
